const express = require('express')
const api = express.Router()
const Model = require('../models/flight.js')
const LOG = require('../utils/logger.js')
const find = require('lodash.find')
const remove = require('lodash.remove')
const notfoundstring = 'No planes found'


// RESPOND WITH VIEWS  --------------------------------------------

// GET to this controller base URI (the default)

api.get('/', (req,res) => {

LOG.info("What have we got here?")
  res.render('planestats/index',{ 
    mean:mean(req.app.locals.planes.query), 
    min: mini(req.app.locals.planes.query), 
    max: maxi(req.app.locals.planes.query)
  })
})

function mean(planes){
  sum = 0
  for(i=0; i < planes.length; i++){
    sum += planes[i].fuelCapacity
  }
  return sum / planes.length
}

function maxi(planes){
  max = -1.0
  for(i=0; i < planes.length; i++){
    if(planes[i].fuelCapacity > max){
      max = planes[i].fuelCapacity
    }
  }
  return max
}

function mini(planes){
  min = 10000
  for(i=0; i < planes.length; i++){
    if(planes[i].fuelCapacity < min){
      min = planes[i].fuelCapacity
    }
  }
  return min
}

module.exports = api
