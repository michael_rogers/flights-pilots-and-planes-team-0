const express = require('express')
const api = express.Router()
const Model = require('../models/flight.js')
const LOG = require('../utils/logger.js')
const find = require('lodash.find')
const remove = require('lodash.remove')
const notfoundstring = 'No pilots found'


// RESPOND WITH VIEWS  --------------------------------------------

// GET to this controller base URI (the default)

api.get('/', (req,res) => {

LOG.info("What have we got here?")
  res.render('pilotstats/index',{ 
    mean:mean(req.app.locals.pilots.query), 
    min: mini(req.app.locals.pilots.query), 
    max: maxi(req.app.locals.pilots.query)
  })
})

function mean(pilots){
  sum = 0
  for(i=0; i < pilots.length; i++){
    sum += pilots[i].hoursFlown
  }
  return sum / pilots.length
}

function maxi(pilots){
  max = -1.0
  for(i=0; i < pilots.length; i++){
    if(pilots[i].hoursFlown > max){
      max = pilots[i].hoursFlown
    }
  }
  return max
}

function mini(pilots){
  min = 10000
  for(i=0; i < pilots.length; i++){
    if(pilots[i].hoursFlown < min){
      min = pilots[i].hoursFlown
    }
  }
  return min
}

module.exports = api
