const express = require('express')
const api = express.Router()
const Model = require('../models/flight.js')
const LOG = require('../utils/logger.js')
const find = require('lodash.find')
const remove = require('lodash.remove')
const notfoundstring = 'No flights found'


// RESPOND WITH VIEWS  --------------------------------------------

// GET to this controller base URI (the default)

api.get('/', (req,res) => {

LOG.info("What have we got here?")
  res.render('flightstats/index',{ 
    mean:mean(req.app.locals.flights.query), 
    min: mini(req.app.locals.flights.query), 
    max: maxi(req.app.locals.flights.query)
  })
})

function mean(flights){
  sum = 0
  for(i=0; i < flights.length; i++){
    sum += flights[i].durationHours
  }
  return sum / flights.length
}

function maxi(flights){
  max = -1.0
  for(i=0; i < flights.length; i++){
    if(flights[i].durationHours > max){
      max = flights[i].durationHours
    }
  }
  return max
}

function mini(flights){
  min = 10000
  for(i=0; i < flights.length; i++){
    if(flights[i].durationHours < min){
      min = flights[i].durationHours
    }
  }
  return min
}

module.exports = api
