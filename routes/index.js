/**
 * @index.js - manages all routing
 *
 * router.get when assigning to a single request
 * router.use when deferring to a controller
 *
 * @requires express
 */

const express = require('express')
const LOG = require('../utils/logger.js')

LOG.debug('START routing')
const router = express.Router()

// Manage top-level request first
router.get('/', (req, res, next) => {
  LOG.debug('Request to /')
  res.render('index.ejs', { title: 'Express App' })
})

// Defer path requests to a particular controller
router.use('/about', require('../controllers/about.js'))
router.use('/puppy', require('../controllers/puppy.js'))
router.use('/flight', require('../controllers/flight.js'))
router.use('/pilot', require('../controllers/pilot.js'))
router.use('/plane', require('../controllers/plane.js'))
router.use('/flightstats', require('../controllers/flightstats.js'))
router.use('/planestats', require('../controllers/planestats.js'))
router.use('/pilotstats', require('../controllers/pilotstats.js'))

LOG.debug('END routing')
module.exports = router
